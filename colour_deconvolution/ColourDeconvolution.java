package colour_deconvolution;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.ImageCanvas;
import ij.gui.ImageWindow;
import ij.process.ImageProcessor;
import toolbox.ImageAccess;

public class ColourDeconvolution {
	public ImageStack[] get_leaf_mask(ImagePlus in) {
	    LinkedHashMap<String,StainMatrix> matrixHashList = null;
	      try {
			matrixHashList = getStainList();
		} catch (IOException e) {
			e.printStackTrace();
		}
		StainMatrix mt = null;
		mt = matrixHashList.get("Brilliant_Blue");
		return mt.compute(false, true, in);
	}
	  static void getmeanRGBODfromROI(int i, double [] rgbOD, ImagePlus imp){
		    //get a ROI and its mean optical density. GL
		    int [] xyzf = new int [4]; //[0]=x, [1]=y, [2]=z, [3]=flags
		    int x1, y1, x2, y2, h=0, w=0, px=0, py=0, x, y,p;
		    double log255=Math.log(255.0);
		    ImageProcessor ip = imp.getProcessor();
		    int mw = ip.getWidth()-1;
		    int mh = ip.getHeight()-1;

		    IJ.showMessage("Select ROI for Colour_"+(i+1)+".\n \n(Right-click to end)");
		    getCursorLoc( xyzf, imp );
		    while ((xyzf[3] & 4) !=0){  //trap until right released
		      getCursorLoc( xyzf, imp );
		      IJ.wait(20);
		    }

		    while (((xyzf[3] & 16) == 0) && ((xyzf[3] & 4) ==0)) { //trap until one is pressed
		      getCursorLoc( xyzf, imp );
		      IJ.wait(20);
		    }

		    rgbOD[0]=0;
		    rgbOD[1]=0;
		    rgbOD[2]=0;

		    if ((xyzf[3] & 4) == 0){// right was not pressed, but left (ROI) was
		      x1=xyzf[0];
		      y1=xyzf[1];
		      //IJ.write("first point x:" + x1 + "  y:" + y1);
		      x2=x1;  y2=y1;
		      while ((xyzf[3] & 4) == 0){//until right pressed
		        getCursorLoc( xyzf, imp );
		        if (xyzf[0]!=x2 || xyzf[1]!=y2) {
		          if (xyzf[0]<0) xyzf[0]=0;
		          if (xyzf[1]<0) xyzf[1]=0;
		          if (xyzf[0]>mw) xyzf[0]=mw;
		          if (xyzf[1]>mh) xyzf[1]=mh;
		          x2=xyzf[0]; y2=xyzf[1];
		          w=x2-x1+1;
		          h=y2-y1+1;
		          if (x2<x1) {px=x2;  w=(x1-x2)+1;} else px=x1;
		          if (y2<y1) {py=y2;  h=(y1-y2)+1;} else py=y1;
		          IJ.makeRectangle(px, py, w, h);
		        }
		        IJ.wait(20);
		      }
		      while ((xyzf[3] & 16) !=0){  //trap until left released
		        getCursorLoc( xyzf, imp );
		        IJ.wait(20);
		      }

		      for (x=px;x<(px+w);x++){
		        for(y=py;y<(py+h);y++){
		          p=ip.getPixel(x,y);
		          // rescale to match original paper values
		          rgbOD[0] = rgbOD[0]+ (-((255.0*Math.log(((double)((p & 0xff0000)>>16)+1)/255.0))/log255));
		          rgbOD[1] = rgbOD[1]+ (-((255.0*Math.log(((double)((p & 0x00ff00)>> 8) +1)/255.0))/log255));
		          rgbOD[2] = rgbOD[2]+ (-((255.0*Math.log(((double)((p & 0x0000ff))        +1)/255.0))/log255));
		        }
		      }
		      rgbOD[0] = rgbOD[0] / (w*h);
		      rgbOD[1] = rgbOD[1] / (w*h);
		      rgbOD[2] = rgbOD[2] / (w*h);
		    }
		    IJ.run("Select None");
		  }


		  static void getCursorLoc(int [] xyzf, ImagePlus imp ) {
		    ImageWindow win = imp.getWindow();
		    ImageCanvas ic = win.getCanvas();
		    Point p = ic.getCursorLoc();
		    xyzf[0]=p.x;
		    xyzf[1]=p.y;
		    xyzf[2]=imp.getCurrentSlice()-1;
		    xyzf[3]=ic.getModifiers();
		  }
		  private LinkedHashMap<String,StainMatrix> getStainList() throws IOException
		  {
		    LinkedHashMap<String,StainMatrix> matrixMap = new LinkedHashMap<String,StainMatrix>();
		    ArrayList<String> lines = new ArrayList<String>();
		    
		    //First, check if the colourdeconvolution.txt exist already
		    String libDir = Constantes.color_deconvolution;
//		    String libDir =IJ.getDirectory("plugins/colourdeconvolution");
		    File file = new File(libDir,"colourdeconvolution.txt");
		    //If not create it
		    if (!file.exists()) {
		      try
		      {
		        InputStream stream = getClass().getResourceAsStream("colourdeconvolution.txt");
		        OutputStream outStream = new FileOutputStream(file);
		        byte[] buffer = new byte[8 * 1024];
		        int bytesRead;
		        while ((bytesRead = stream.read(buffer)) != -1)
		          outStream.write(buffer, 0, bytesRead);
		        stream.close();
		        outStream.close();
		      }
		      catch(IOException ioe)
		      {
		        IJ.error("Plugin Directory not writable", "The Plugin directory is not writable, so the file containing the vector list would not be copied into the plugin directory."+
		                                                   System.getProperty("line.separator")+ioe);
		        if(file.exists())
		          file.delete();
		      }
		    }
		    
		    if (!file.exists())
		    {
		      //First, read the colourdeconvolution.txt included in the jar file
		      InputStream stream = getClass().getResourceAsStream("colourdeconvolution.txt");
		      BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		      String line = reader.readLine();
		      while (line != null) {
		        lines.add(line);
		        line = reader.readLine();
		      }
		      reader.close();    
		      for(int i=1;i<lines.size();i++)
		      {
		        StainMatrix matrix = new StainMatrix();
		        matrix.init(lines.get(i));
		        matrixMap.put(matrix.myStain, matrix);
		      }
		    }
		    else
		    {    
		      BufferedReader reader = new BufferedReader(new FileReader(file));
		      String line = reader.readLine();
		      while (line != null) {
		        lines.add(line);
		        line = reader.readLine();
		      }
		      reader.close();
		      for(int i=1;i<lines.size();i++)
		      {
		        StainMatrix matrix = new StainMatrix();
		        matrix.init(lines.get(i));
		        matrixMap.put(matrix.myStain, matrix);
		      }
		    }
		    

		    return matrixMap;
		  }
}
