package colour_deconvolution;

import java.io.File;

public class Constantes {
	/*
	 * Calibration pictures directory path
	 */
	public static String color_deconvolution = System.getProperty("user.home") + File.separator + "EPFL" + File.separator 
			+ "M2-EPFL" + File.separator + "Semester_Project" + File.separator + "ImageJ" + File.separator
			+ "plugins" + File.separator + "IP_Stereo" + File.separator + "colour_deconvolution" + File.separator;
}