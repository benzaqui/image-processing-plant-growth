package gradientdescent;

import java.util.ArrayList;

import calibration.PointInter;
import toolbox.EllipseTemplate;
import toolbox.ImageAccess;

public class GradientHelper {
	/** An example function with a form of y = a0 * x0 + a1 * x1 + a2*/
	public static class LeafFunction{

		/**
		 * 
		 */
		public double getY( double a[], ImageAccess[] images) {
			int windowx = (int)a[2]*2+2;
			double[][] window = images[0].getNeighborhood((int)a[0],(int)a[1], windowx,  windowx);
			EllipseTemplate el = new EllipseTemplate(a[2],a[2]/a[3],a[4]);
			ArrayList<PointInter> edge = EllipseTemplate.get_edge_pixels(el);
			double cost = 0;
			for (PointInter p:edge) {
				cost += window[windowx/2+p.x-1][windowx/2+p.y-1]/images[0].getMaximum()*images[1].getPixel((int)a[0], (int)a[1]);
			}
			cost /= edge.size();
			return cost;
		}
		/** 
		 * The method which gives the partial derivates used in the LMA fit.
		 */
		public double getPartialDerivate(double a[], int parameterIndex, ImageAccess[] images) {
			double da = 1;
			double[] a_plus = a;
			double[] a_minus = a;
			if (parameterIndex <=4 ) {
	        	a_plus[parameterIndex] += da;
	        	a_minus[parameterIndex] -= da;
	            return getY(a_plus, images)-getY(a_minus, images);
	        }
			else{
				throw new RuntimeException((new StringBuilder("No such parameter index: ")).append(parameterIndex).toString());
			}
		}
	}
}
