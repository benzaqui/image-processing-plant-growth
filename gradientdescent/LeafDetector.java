package gradientdescent;

import java.awt.Color;
import java.util.ArrayList;

import calibration.PointInter;
import ij.ImagePlus;
import ij.gui.OvalRoi;
import ij.gui.Overlay;
import toolbox.EllipseTemplate;
import toolbox.ImageAccess;

public class LeafDetector {
	public static long GradientInit(ImageAccess mask, ImageAccess ridge_im, ImagePlus imp){
		ArrayList<Params> best_params = new ArrayList<Params>();
		double best_cost = 0;
		ArrayList<PointInter> best = new ArrayList<PointInter>();
		
		double max_ridge = ridge_im.getMaximum();
		
		int step_x = 5;
		int step_y = 5;
		int min_a = 60;
		int max_a = 150;
		int step_a = 30;
		double min_scale = 1.0;
		double max_scale = 2;
		double step_scale = 0.5;
		long count = 0;
		
		
		for(int x=450; x<mask.nx;x+=step_x)
			for(int y=450; y<mask.ny;y+=step_y) {
				if (mask.getPixel(x, y) == 1) {
					for (double a=min_a; a<=max_a;a+=step_a) {
						for (double scale=min_scale; scale<=max_scale;scale+=step_scale) {
							int windowx = (int)a*2+2;
							if (x-windowx > 0 && x+windowx<mask.nx && y-windowx>=0 && y+windowx <mask.ny) {
								double[][] window = ridge_im.getNeighborhood(x,y, windowx,  windowx);
								for (double angle=-Math.PI/2.0; angle<Math.PI/2.0; angle+=Math.PI/12) {
									EllipseTemplate el = new EllipseTemplate(a,a/scale,angle);
									ArrayList<PointInter> edge = EllipseTemplate.get_edge_pixels(el);
	//								ArrayList<PointInter> ridge = EllipseTemplate.get_ridge_pixels(el);
									double cost = 0;
									for (PointInter p:edge) {
										cost += window[windowx/2+p.x-1][windowx/2+p.y-1]/max_ridge;
									}
									cost /= edge.size();
//									IJ.log("Cost: " + cost);
									if (cost > 0.05) {
//										IJ.log("Cost: " + cost);
//										IJ.log( "x: " + x + " y: " + y + " a: " + a
//												+ " b: " + a/scale + " angle: " + angle*180/Math.PI + " ");
										for (PointInter p:edge) {
											best.add(p);
											best_params.add(new Params(x, y, (int)a, scale, angle));
										}
										best_cost = cost;
									}
				//					EllipseTemplate.draw_pixels(ridge, im);
				//					EllipseTemplate.draw_pixels(edge, im);
									count++;
								}
							}
						}
					}
				}
			}
		Overlay overlay = new Overlay();
		for (Params p:best_params) {
			OvalRoi inter_oval = new OvalRoi(p.x, p.y, p.a, p.scale*p.a);
			inter_oval.setStrokeColor(Color.blue);
			overlay.add(inter_oval);
			}
		imp.setOverlay(overlay);
		imp.show();
		return count;
	}
	/*
	 * 
	 */
	public static void GradientDescent( double stop_value, double[] initialization , int n_iter) {
		int count = 0;
		while (stop_value >= 1 && count <=n_iter) {
			//calculate cost function
			//calculate gradient in all params
			//update params according to gradient
			//count ++
		}
	}
}
