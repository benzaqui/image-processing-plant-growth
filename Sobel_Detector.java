import java.io.File;

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.PlugIn;
import ij.process.ImageProcessor;

public class Sobel_Detector implements PlugIn{

	//private String desktop = System.getProperty("user.home") + File.separator + "Desktop" + File.separator;
	@Override
	public void run(String arg0) {
	
		ImagePlus imp = IJ.getImage();
		//ImagePlus imp = IJ.openImage(desktop + "image2018-10-28-174941.jpg"); //load image from desktop
		if (imp == null) {
			IJ.error("Not found");
			return;
		}
		IJ.run(imp, "8-bit", "");
		int nx = imp.getWidth();
		int ny = imp.getHeight();
		ImagePlus out_sobel = IJ.createImage("out_sobel", nx, ny, 1, 32);
		ImageProcessor ip = imp.getProcessor();
		int n=3;
		double b[][] = new double[n][n];
		//Add histogram des angles pour definir 
		double maskx[][] = {{-1,0,1},{-2,0,2},{-1,0,1}};
		double masky[][] = {{-1,-2,-1},{0,0,0},{1,2,1}};
		for(int i=0;i<n;i++)
			for(int j=0; j<n; j++) {
				IJ.log(" " + maskx[i][j]);
			}
		for(int x=1; x<nx-1; x++)
			for(int y=1; y<ny-1; y++) {
				ip.getNeighborhood(x, y, b);
				double sobel_x = 0;
				double sobel_y = 0;
				for(int i=0;i<n;i++)
					for(int j=0; j<n; j++) {
						sobel_x += maskx[i][j]*b[i][j];
						sobel_y += masky[i][j]*b[i][j];
					}
				out_sobel.getProcessor().putPixelValue(x, y, Math.sqrt(sobel_x*sobel_x+sobel_y*sobel_y));
			}	
		out_sobel.getProcessor().resetMinAndMax();
		out_sobel.show();
		
	}
}

