import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.PointRoi;
import ij.gui.Roi;
import ij.io.FileSaver;
import ij.plugin.PlugIn;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

import toolbox.ImageAccess;
import toolbox.ImageTools;
import toolbox.Vector3D;
import toolbox.MorphologicTransform;
import toolbox.MorphologicTransform.Shape;
import toolbox.HysteresisThreshold;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import delaunay.DelaunayTriangulator;
import delaunay.NotEnoughPointsException;
import delaunay.Triangle2D;
import delaunay.Vector2D;

public class Depth_Map implements PlugIn{

	private String calibration = System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "calibration_simple" + File.separator + "pictures_verif" + File.separator;
	private String images = System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "Images_from_dropbox" + File.separator;
	private String save_dir = System.getProperty("user.home") + File.separator + "EPFL" + File.separator + "M2-EPFL" + File.separator + "Semester_project" + File.separator + "TIMELIPSE_PICTURES" + File.separator;
	double pix_size = 0.00246; //in dm
//	boolean print = false;
	
	/*
	 * (non-Javadoc)
	 * @see ij.plugin.PlugIn#run(java.lang.String)
	 * 
	 * Compute Depth Map from 2 stereo images of plants
	 */
	@Override
	public void run(String arg0) {
		
		ArrayList<Vector2D> surfaces = new ArrayList<Vector2D>();
		ArrayList<Vector2D> volumes = new ArrayList<Vector2D>();
		ArrayList<Vector2D> nb_leafs = new ArrayList<Vector2D>();
		
		for (int jour = 23; jour<24 ; jour++) {
			String day = null;
			if (jour<10)
				day = "0" + jour + "_12_2018"+ File.separator;
			else
				day = jour + "_12_2018"+ File.separator;
			for (int image = 13; image<14 ; image++) {
			
				String image1 = image + "_cam1.png";
				String image2 = image + "_cam2.png";
				
				IJ.open(images + day + image1);
				IJ.open(images + day + image2);
				
				IJ.run("Extract SIFT Correspondences", "source_image=" + image1 +" target_image=" + image2 + " initial_gaussian_blur=1.6 steps_per_scale_octave=8 minimum_image_size=64 maximum_image_size=1600 feature_descriptor_size=5 feature_descriptor_orientation_bins=8 closest/next_closest_ratio=2 filter maximal_alignment_error=200 minimal_inlier_ratio=0.50 minimal_number_of_inliers=7 expected_transformation=Rigid");;
				IJ.log(" Image 1: ");
				ImagePlus imp = WindowManager.getImage(image1);
				int nx = imp.getWidth();
				int ny = imp.getHeight();
				ImageAccess depth = new ImageAccess( nx, ny);
				int xpoints[] = null;
				int ypoints[] = null;
				Roi rois = imp.getRoi();
				if (rois instanceof PointRoi) {
					xpoints = rois.getPolygon().xpoints;
					ypoints = rois.getPolygon().ypoints;
				}
			//		for (int i=0; i< xpoints.length; i++) {
			//			IJ.log("pts x: " + xpoints[i] + "pts y: " + ypoints[i]);
			//		}
				IJ.log(" Image 2: ");
				ImagePlus imp2 = WindowManager.getImage(image2);
				
				int xpoints2[] = null;
				int ypoints2[] = null;
				Roi rois2 = imp2.getRoi();
				if (rois instanceof PointRoi) {
					xpoints2 = rois2.getPolygon().xpoints;
					ypoints2 = rois2.getPolygon().ypoints;
				}
				/*
				 * Compute depth of each Pixel of interest
				 */
				for (int i=0; i< xpoints.length; i++) {
					IJ.log("pts x: " + xpoints2[i] + "pts y: " + ypoints2[i]);
					double disparity = Math.abs(xpoints2[i]-xpoints[i]);
					//magic number from calibration 
					double z = -0.0061*disparity*disparity+3.5328*disparity-366.45;
					if (z>=-50 && z < 250)
						depth.putPixel(xpoints[i], ypoints[i], z);
				}
				//show map of pts
			//		depth.show();
				
				
				IJ.run(imp, "Colour Deconvolution", "vectors=Brilliant_Blue");
				IJ.selectWindow(image1+"-(Colour_1)");
				ImagePlus im_bis = WindowManager.getCurrentImage();
				ImagePlus im = im_bis.duplicate();
				IJ.run(im, "8-bit", "");
				ImageAccess mask = new ImageAccess(im);
				mask = ImageTools.threshold(mask, 170);
//				mask.show("Thresholded");
				ImageAccess bot = new ImageAccess();
				mask = MorphologicTransform.open(mask,Shape.CROSS, 4);
				bot = MorphologicTransform.bottomHat(mask, Shape.CROSS, 10);
				mask.show("opened");
				//save mask
				FloatProcessor mask_out = mask.createFloatProcessor();
				ImageProcessor mask_ip = mask_out.convertToFloat();
				ImagePlus out_mask = new ImagePlus();
				out_mask.setProcessor(mask_ip);
				FileSaver save_mask = new FileSaver(out_mask);
				save_mask.saveAsPng(save_dir + "canopy" + File.separator + day);
				
				//save canopy surface of the plant
				double surf = (1-mask.getMean()/255.0)*mask.getHeight()*mask.getWidth()*pix_size*pix_size*100;
				surfaces.add(new Vector2D(jour, surf));
				IJ.log("Surface: " + surf + " cm^2");
				
			//		bot.show("BottomHat");
				
				depth = set_depth(depth, mask);
			
				Vector<Vector2D> pointSet = background_pts(bot);
				for(int i =0; i<xpoints.length;i++) {
					pointSet.addElement(new Vector2D(xpoints[i], ypoints[i]));
				}
				IJ.log("Show pts");
				for (Vector2D p: pointSet) {
					p.toString();
				}
					
				DelaunayTriangulator delaunayTriangulator = new DelaunayTriangulator(pointSet);
				try {
					delaunayTriangulator.triangulate();
			
					List<Triangle2D> triangles = delaunayTriangulator.getTriangles();
					IJ.log("nb triangles"+triangles.size());
					
					ImageAccess depth_map = depth_map(triangles, depth);
					double volume = get_volume(depth_map);
					volumes.add(new Vector2D(jour, volume));
			//			depth_map.show();
					FloatProcessor out = depth_map.createFloatProcessor();
					ImageProcessor ip = out.convertToFloat();
					ImagePlus out_depth = new ImagePlus();
					out_depth.setProcessor(ip);
//					WindowManager.closeAllWindows();
					out_depth.show("Depth Map");
					FileSaver save_depth = new FileSaver(out_depth);
					save_depth.saveAsPng(save_dir + "depth_map" + File.separator + day);
					IJ.run("3D Surface Plot","plotType=3  drawLines=0 smooth=20 backgroundColor=FFFFFF lineColor=000000 "
							+ "colorType=0 grid=1024 snapshot=1 colorType=3 rotationZ="+30);
					FileSaver save3d = new FileSaver(WindowManager.getCurrentImage());
					save3d.saveAsPng(save_dir + "3d_view" + File.separator + day);
				} catch (NotEnoughPointsException e) {
					e.printStackTrace();
				}
			}
		}
		IJ.log("Surfaces: ");
		for (Vector2D p : surfaces) {
			IJ.log(p.x + ", " + p.y);
		}
		IJ.log("Volumes: ");
		for (Vector2D p : volumes) {
			IJ.log(p.x + ", " + p.y);
		}
	}
	
	double get_volume(ImageAccess in) {
		double depth = 0;
		double temp;
		for (int i=0;i<in.nx;i++)
			for (int j=0;j<in.ny;j++) {
				temp = in.getPixel(i, j);
				if (Double.isNaN(temp)) {
				}else {
					depth += temp;
				}
			}
		double width = pix_size * in.nx;
		double height = pix_size * in.ny;
		depth/= (double) (in.nx*in.ny);
		IJ.log(" depth mean across image (in mm): "+ depth);
		
		double volume = depth*width*height*0.01;
		IJ.log(" Volume (litres): "+ volume);
		return volume/ (double) (in.nx*in.ny);
	}
	
	/*
	 * TODO: Change using color deconvolution plugin to define ROI
	 * selectWindow("1.png");
	 */
	public ImageAccess set_depth(ImageAccess in, ImageAccess mask) {
		for (int i=0; i< in.nx ; i++)
			for (int j=0; j< in.ny ; j++)
				if (mask.getPixel(i, j) == 255)
					in.putPixel(i, j, 0);
		return in;
	}
	public Vector<Vector2D> background_pts(ImageAccess imp) {
		Vector<Vector2D> points = new Vector<Vector2D>();
		int n = 0;
		for (int i=0; i< imp.nx ; i+=4)
			for (int j=0; j< imp.ny ; j+=4)
				if (imp.getPixel(i, j) == 255 || imp.getPixel(i, j) == -255) {
					n++;
					points.addElement(new Vector2D(i, j));
				}
		IJ.log(n + " elements added");
		return points;
	}
	
	public ImageAccess depth_map(List<Triangle2D> triangles, ImageAccess in) {
		ImageAccess depth = new ImageAccess(in.nx, in.ny);
		for(int i =0; i<triangles.size();i++) {
			//IJ.log(triangles.get(i).toString());
			Vector2D a = triangles.get(i).a;
			Vector2D b = triangles.get(i).b;
			Vector2D c = triangles.get(i).c;
			Vector3D a3 = conversion2d_3d(a,in.getPixel((int)a.x, (int)a.y));
			Vector3D b3 = conversion2d_3d(b,in.getPixel((int)b.x, (int)b.y));
			Vector3D c3 = conversion2d_3d(c,in.getPixel((int)c.x, (int)c.y));
			for (int j=min_x(triangles.get(i));j<max_x(triangles.get(i));j++)
				for (int k=min_y(triangles.get(i));k<max_y(triangles.get(i));k++) {
					Vector2D pt = new Vector2D(j,k);
					if (PointInTriangle(pt, a, b, c)) {
						double intensity = interp3points(pt, a3, b3, c3);
						if (intensity <=0)
							depth.putPixel(j, k, 0);
						else
							depth.putPixel(j, k, intensity);
						
						//IJ.log("put pixel: " + j + " " + k + " " +  interp3points(pt, a3, b3, c3));
					}
				}
		}
		return depth;
	}
	/*
	 * Interpolation of third dimension from triangle
	 * Maybe error here
	 */
	public double interp3points(Vector2D v, Vector3D a, Vector3D b, Vector3D c) {
		
		double weighta = ((b.y-c.y)*(v.x-c.x)+(c.x-b.x)*(v.y-c.y))/((b.y-c.y)*(a.x-c.x)+(c.x-b.x)*(a.y-c.y));
		double weightb = ((c.y-a.y)*(v.x-c.x)+(a.x-c.x)*(v.y-c.y))/((b.y-c.y)*(a.x-c.x)+(c.x-b.x)*(a.y-c.y));
		double weightc = 1-weighta-weightb;
		return (weighta*a.z+weightb*b.z+weightc*c.z);
	}
	
	public int max_x(Triangle2D triangle) {
		return (int)Math.max(Math.max(triangle.a.x, triangle.b.x), Math.max(triangle.b.x, triangle.c.x));
	}
	
	public int max_y(Triangle2D triangle) {
		return (int)Math.max(Math.max(triangle.a.y, triangle.b.y), Math.max(triangle.b.y, triangle.c.y));
	}
	
	public int min_x(Triangle2D triangle) {
		return (int)Math.min(Math.min(triangle.a.x, triangle.b.x), Math.min(triangle.b.x, triangle.c.x));
	}
	
	public int min_y(Triangle2D triangle) {
		return (int)Math.min(Math.min(triangle.a.y, triangle.b.y), Math.min(triangle.b.y, triangle.c.y));
	}
	
	public double sign (Vector2D p1, Vector2D p2, Vector2D p3)
	{
	    return ((p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y));
	}
	
	public boolean PointInTriangle (Vector2D pt, Vector2D v1, Vector2D v2, Vector2D v3)
	{
	    double d1, d2, d3;
	    boolean has_neg, has_pos;

	    d1 = sign(pt, v1, v2);
	    d2 = sign(pt, v2, v3);
	    d3 = sign(pt, v3, v1);

	    has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
	    has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

	    return !(has_neg && has_pos);
	}
	
    public Vector3D conversion2d_3d(Vector2D vector, double z) {
    	Vector3D out = new Vector3D(vector.x, vector.y, z);
    	return out;
    }


}
