package calibration;

public class PointAcc implements Comparable<PointAcc> {
	public int r;
	public int a;
	public int acc;
	private boolean sortRadius = false;
	
	public PointAcc(int a, int r, double acc) {
		this.r = r;
		this.a = a;
		this.acc = (int)acc;
	}
	
	public void sortByRadius() {
		sortRadius = true;
	}

	public void sortByAccumulator(){
		sortRadius = false;
	}

	@Override
	public int compareTo(PointAcc pointacc) {
		if (sortRadius == true){
			return this.r - pointacc.r;
		}else{
			return pointacc.acc - this.acc;
		}
	}
}