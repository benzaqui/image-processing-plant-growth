package calibration;

import java.awt.Color;
import java.util.ArrayList;

import ij.gui.Line;
import ij.gui.OvalRoi;
import ij.gui.Overlay;

public class Visualization {
	public static Overlay show_poi(Overlay overlay, ArrayList<PointAcc> list, Color color) {
		for(PointAcc p : list) {
			OvalRoi inter_oval = new OvalRoi(p.a-3, p.r-3, 7, 7);
			inter_oval.setStrokeColor(color);
			overlay.add(inter_oval);
		}
		return overlay;
	}
	public static Overlay show_intersect(Overlay overlay, ArrayList<PointInter> list, Color color) {
		for(PointInter p : list) {
			OvalRoi inter_oval = new OvalRoi(p.x-3, p.y-3, 7, 7);
			inter_oval.setStrokeColor(color);
			inter_oval.setFillColor(new Color(120, 255, 120, 150));
			overlay.add(inter_oval);
			//IJ.log("pt " + p.x + " " + p.y );
		}
		return overlay;
	}
	public static Overlay show_lines(Overlay overlay, ArrayList<PointAcc> list, int nx, int ny, Color color) {
		overlay.setStrokeColor(color);
		for (PointAcc l : list) {
			double theta = (l.a-90*Constantes.R_FACTOR_HOUGH) * Math.PI/ (180.0*Constantes.R_FACTOR_HOUGH);
			if (theta > Math.PI/4 && theta < Math.PI*3/4) {
				int y2 = (int)Math.round((l.r-Math.cos(theta)*nx)/Math.sin(theta));
				int y1 =(int)Math.round((l.r)/Math.sin(theta));
				overlay.add(new Line(0, y1, nx, y2));
			}else {
				int x2 = (int)Math.round((l.r-Math.sin(theta)*ny)/Math.cos(theta));
				int x1 =(int)Math.round((l.r)/Math.cos(theta));
				overlay.add(new Line(x1, 0, x2, ny));
			}
		}
		return overlay;
	}
}
