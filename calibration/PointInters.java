package calibration;

import java.util.ArrayList;

import calibration.PointInter;

public class PointInters extends ArrayList<PointInter> {
	
	public double getMeanX() {
		double mx = 0;
		for(PointInter p : this)
			mx += p.x;
	
	return mx/this.size();
	}
}