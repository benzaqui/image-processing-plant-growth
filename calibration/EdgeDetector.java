package calibration;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;

public class EdgeDetector {
	
	public static final ImagePlus sobel_filter(ImagePlus imp) {
		IJ.run(imp, "8-bit", "");
		int nx = imp.getWidth();
		int ny = imp.getHeight();
		ImagePlus out_sobel = IJ.createImage("", nx, ny, 1, 32);
		ImageProcessor ip = imp.getProcessor();
		int n=3;
		double b[][] = new double[n][n];
		double maskx[][] = {{-1,0,1},{-2,0,2},{-1,0,1}};
		double masky[][] = {{-1,-2,-1},{0,0,0},{1,2,1}};
		for(int x=1; x<nx-1; x++)
			for(int y=1; y<ny-1; y++) {
				ip.getNeighborhood(x, y, b);
				double sobel_x = 0;
				double sobel_y = 0;
				for(int i=0;i<n;i++)
					for(int j=0; j<n; j++) {
						sobel_x += maskx[i][j]*b[i][j];
						sobel_y += masky[i][j]*b[i][j];
					}
				out_sobel.getProcessor().putPixelValue(x, y, Math.sqrt(sobel_x*sobel_x+sobel_y*sobel_y));
			}	
		if (Constantes.SHOW_INTER_RESULTS == true) {
			out_sobel.getProcessor().resetMinAndMax();
			out_sobel.show();
		}
		return out_sobel;
	}
}
