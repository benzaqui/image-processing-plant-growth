package calibration;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.Line;
import ij.gui.OvalRoi;
import ij.gui.Overlay;
import ij.plugin.filter.GaussianBlur;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;
import toolbox.MathTools;
import calibration.Constantes.Direction;
import calibration.PointAcc;
import calibration.PointInter;
import calibration.PointInters;

public class HoughTransform {
	
	
	
	/*
	 * Apply Line Detection and return positions of intersection of type
	 * 
	 */
	public static ArrayList<PointInter> hough(ImagePlus imp, double t, int hough_theta, int hough_r, int concentration, 
			int nb_vertical_lines, int nb_horizontal_lines, int angle_var) {
		
				int nx = imp.getWidth(); 
				int ny = imp.getHeight();
				float acc[][] = new float[hough_theta][hough_r]; //dimensions of Hough Transform 
				ImageProcessor ip = imp.getProcessor();
				for(int i=0; i<nx; i++) 
				for(int j=0; j<ny; j++) {
					float v = ip.getPixelValue(i, j);
					if (v > t)
						cumul(acc, i, j, v);
				}
			// BLURRING
				//ImageProcessor fp = new FloatProcessor(acc);
				FloatProcessor fp_blur = new FloatProcessor(acc);
				GaussianBlur gb = new GaussianBlur();
				gb.blurGaussian(fp_blur, 2, 2, 6);
				//ImagePlus hough = new ImagePlus("Hough", fp);
				ImagePlus hough_blur = new ImagePlus( imp.getTitle() +" -> Hough", fp_blur);

			//FIND LOCAL MAXIMUMS
				ArrayList<PointAcc> list = localMax(hough_blur);	
			//KEEP MORE INTENSE PT IN NEIGHBOORHOOD 
				point_concentration(list, concentration);
			//SORT LIST BY INTENSITIES
				for (PointAcc p :list)
					p.sortByAccumulator();
				Collections.sort(list);
			//SEPARE LIST IN ANGLE>MEAN AND ANGLE<MEAN
				ArrayList<PointAcc> poi_left = list_separation(list, angle_average(list), Direction.LEFT, nb_vertical_lines);
				ArrayList<PointAcc> poi_right = list_separation(list, angle_average(list), Direction.RIGHT, nb_horizontal_lines);
			//KEEP PARALLEL LINES IF ERROR FROM MEAN < 7
				ArrayList<PointAcc> lines_left = poi_left;//keep_parallel(poi_left, angle_var); // 7 degree variation acceptation
				ArrayList<PointAcc> lines_right = poi_right; //keep_parallel(poi_right, angle_var);
			// SORT PARAMETRIC LINES BY RADIUS
				for (PointAcc p :lines_left)
					p.sortByRadius();
				for (PointAcc p :lines_right)
					p.sortByRadius();
				Collections.sort(lines_left);
				Collections.sort(lines_right);
			// LIST INTERSECTION PTS IN ORDER
				ArrayList<PointInter> inter = intersection(lines_left, lines_right);
					
				//hough.show();
				if (Constantes.SHOW_INTER_RESULTS == true) {
					hough_blur.getProcessor().resetMinAndMax();
					hough_blur.show();
				}
					
				if (Constantes.SHOW_OVERLAYS == true) {
					Overlay imp_overlay = new Overlay();
					imp_overlay = Visualization.show_lines(imp_overlay, lines_left, nx, ny, Color.blue);
					imp_overlay = Visualization.show_lines(imp_overlay, lines_right, nx, ny, Color.green);
				// SHOW INTERSECTION PTS IN IMAGE
					imp_overlay = Visualization.show_intersect(imp_overlay, inter, Color.blue);
				// HIGHLIGHT POINT OF INTEREST HOUGH TRANSFORM
					Overlay hough_overlay = new Overlay();
					hough_overlay = Visualization.show_poi(hough_overlay, lines_left, Color.blue);
					hough_overlay = Visualization.show_poi(hough_overlay, lines_right, Color.green);
					hough_blur.setOverlay(hough_overlay);
					imp.getProcessor().resetMinAndMax();
					imp.setOverlay(imp_overlay);
				}
		return inter;
	}
	public static ArrayList<PointAcc> keep_parallel(ArrayList<PointAcc> list, float max_angle) {
		ArrayList<PointAcc> new_list = new ArrayList<PointAcc>();
		float angle_average = angle_average(list);
		for(PointAcc p : list) {
			if (Math.abs(p.a - angle_average) < max_angle)
				new_list.add(new PointAcc(p.a, p.r, p.acc));
		}
		return new_list;
	}
	public static float angle_average(ArrayList<PointAcc> list){
		float angle_average = 0;
		for(PointAcc p : list)
			angle_average+= (float)p.a;
		angle_average = angle_average/list.size();
		return angle_average;
	}
	
	public static ArrayList<PointAcc> list_separation(ArrayList<PointAcc> list, float mean_angle, Direction dir, int max_lines) {
		ArrayList<PointAcc> new_list = new ArrayList<PointAcc>();
		for(PointAcc p : list) {
			if ( dir == Direction.RIGHT)
				if (p.a > mean_angle && max_lines > 0) {
					new_list.add(new PointAcc(p.a, p.r, p.acc));
					max_lines--;
				}
			if ( dir == Direction.LEFT)
				if (p.a < mean_angle && max_lines > 0) {
					new_list.add(new PointAcc(p.a, p.r, p.acc));
					max_lines--;
				}
		}
		if (max_lines > 0)
			IJ.log("ERROR: Not enough lines detected");
		return new_list;
	}
	
	/*
	 * Faux mais fonctionne...
	 */
	private static void point_concentration(ArrayList<PointAcc> list, float distance) {
		int w = list.size();
		int i = 0;
		MathTools tool = new MathTools();
		while (i < w) {
			int j = i+1;
			while (j < w) {
				PointAcc p1 = list.get(i);
				PointAcc p2 = list.get(j);
				if (tool.norm2(p2.a-p1.a, p2.r-p1.r) < distance) {
					if (p1.acc > p2.acc) {
						list.remove(j);
						j--;
					}else {
						list.remove(i);
						j=i;
					}
					w--;
				}
				j++;
			}
			i++;
		}
	}
	
	private static void cumul(float[][] acc, int x, int y, float value) {
		int na = acc.length;
		int nr = acc[0].length;
		for(int a=0; a<na; a++) {
			double theta = (a/Constantes.R_FACTOR_HOUGH-90) * Math.PI / 180.0;
			double r = (x * Math.cos(theta) + y * Math.sin(theta));
			if (r >= 0 && r < nr) {
				int ri = (int)Math.round(r);
				acc[a][ri]+= value;
			}
		}
	}

	// ADD hough transform interesting pixels
	public static ArrayList<PointAcc> localMax(ImagePlus imp) {

		int nx = imp.getWidth();
		int ny = imp.getHeight();
		ImageProcessor ip = imp.getProcessor();
		ArrayList<PointAcc> list = new ArrayList<PointAcc>();	
		int threshold = 0;
			for (int x = 1; x < nx - 1; x++)
				for (int y = 1; y < ny - 1; y++) {
					double v = ip.getPixelValue(x, y);
					if (v > threshold)
						if (v > ip.getPixelValue(x - 1, y - 1)) {
							if (v > ip.getPixelValue(x - 1, y)) {
								if (v > ip.getPixelValue(x - 1, y + 1)) {
									if (v > ip.getPixelValue(x, y - 1))  {
										if (v > ip.getPixelValue(x, y + 1))  {
											if (v > ip.getPixelValue(x + 1, y - 1))  {
												if (v > ip.getPixelValue(x + 1, y))  {
													if (v > ip.getPixelValue(x +1 , y + 1))  {
														list.add(new PointAcc(x, y, v));
													}
												}
											}
										}
									}
								}
							}
						}
					}
		return list;
	}
	//ADD lines intersection positions x, y
	public static PointInters intersection(ArrayList<PointAcc> line1, ArrayList<PointAcc> line2) {
		PointInters inter = new PointInters();
		double r1 = 0;
		double theta1 = 0;
		double r2 = 0;
		double theta2 = 0;
		int x = 0;
		int y = 0;
		for (PointAcc p : line1) {
			for(PointAcc q : line2) {
				r1 = p.r;
				theta1 = (p.a-90*Constantes.R_FACTOR_HOUGH)*Math.PI/(180.0*Constantes.R_FACTOR_HOUGH);
				r2 = q.r;
				theta2 = (q.a-90*Constantes.R_FACTOR_HOUGH)*Math.PI/(180.0*Constantes.R_FACTOR_HOUGH);
				x = (int)Math.round((r1*Math.sin(theta2) - r2*Math.sin(theta1))/(Math.cos(theta1)*Math.sin(theta2) - 
						Math.cos(theta2)*Math.sin(theta1)));
				if (theta1 < 0.1) {
					y = (int)(Math.round(-x/Math.tan(theta2)+r2/Math.sin(theta2)));
				}else {
					y = (int)(Math.round(-x/Math.tan(theta1)+r1/Math.sin(theta1)));
				}
				inter.add(new PointInter (x, y));
			}
		}
		return inter;
	}
}
