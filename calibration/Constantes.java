package calibration;

import java.io.File;

public class Constantes {
	
	public enum Dir{CALIB};
	public enum Table{CALIB};
	public enum Bool{SHOW_FINAL_RESULTS, SHOW_ALL_RESULTS, SHOW_NOTHING}
	public enum Direction {RIGHT, LEFT}
	
	/*
	 * Define constantes and Booleans of project
	 */
	
	public static boolean SHOW_OVERLAYS = false;
	public static boolean SHOW_INTER_RESULTS = false;
	public static boolean SHOW_FINAL_RESULTS = false;
	public static boolean COMPUTE_TIME = false;
	
	public static double R_FACTOR_HOUGH = 2.0;
	
	/*
	 * Calibration pictures directory path
	 */
	private static String calibration = System.getProperty("user.home") + File.separator + "Desktop" + File.separator 
			+ "calibration_simple" + File.separator;
	
	
	/*
	 * Initialization of calibration distances related to pictures
	 */
	private static final int[][] calib(){
		
		int calib[][] = new int[12][3];
		calib[0][2] = 10;
		calib[1][2] = 20;
		calib[2][2] = 30;
		calib[3][2] = 40;
		calib[4][2] = 60;
		calib[5][2] = 70;
		calib[6][2] = 80;
		calib[7][2] = 90;
		calib[8][2] = 100;
		calib[9][2] = 110;
		calib[10][2] = 120;
		calib[11][2] = 130;
		return calib;
	}
	
	public static final void set_bool(Bool param) {
		if (param == Bool.SHOW_ALL_RESULTS) {
			SHOW_OVERLAYS = true;
			SHOW_INTER_RESULTS = true;
			SHOW_FINAL_RESULTS = true;
			COMPUTE_TIME = false;
		}
		else if (param == Bool.SHOW_FINAL_RESULTS){
			SHOW_OVERLAYS = true;
			SHOW_INTER_RESULTS = false;
			SHOW_FINAL_RESULTS = true;
			COMPUTE_TIME = true;
		}
		else if (param == Bool.SHOW_NOTHING) {
			SHOW_OVERLAYS = false;
			SHOW_INTER_RESULTS = false;
			SHOW_FINAL_RESULTS = false;
			COMPUTE_TIME = true;
		}
		else {
			return;
		}
	}
	
	public static final String get_dir(Dir param){
		if (param == Dir.CALIB)
			return calibration;
		
		
		return null;
	}
	
	public static final int[][] get_table(Table param){
		if (param == Table.CALIB)
			return calib();
		
		return null;
	}
}
