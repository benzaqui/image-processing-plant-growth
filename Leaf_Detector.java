import java.awt.Color;
import java.io.File;
import java.util.ArrayList;

import calibration.PointInter;
import colour_deconvolution.ColourDeconvolution;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.OvalRoi;
import ij.gui.Overlay;
import ij.plugin.PlugIn;
import ij.plugin.filter.GaussianBlur;
import ij.process.FloatProcessor;
import toolbox.EllipseTemplate;
import toolbox.ImageAccess;
import toolbox.ImageTools;
import toolbox.MorphologicTransform;
import toolbox.MorphologicTransform.Shape;
import toolbox.RidgeDetector;
import gradientdescent.LeafDetector;

/*
 * Apply gradient descent to find best fit of ellipse to detect leafs of salad/basilic
 */
public class Leaf_Detector implements PlugIn{

	boolean DESCENT = true;
	private String calibration = System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "calibration_simple" + File.separator + "pictures_verif" + File.separator;

	@Override
	public void run(String arg0) {
		ImagePlus imp = IJ.openImage(calibration  +"1.png");
//		ImageAccess in = new ImageAccess(imp);
		ImageAccess ridge_im = new ImageAccess(IJ.openImage(calibration  +"ridge.png"));
//		ColourDeconvolution mask1 = new ColourDeconvolution();
//		GaussianBlur gauss = new GaussianBlur();
//		gauss.blurGaussian(imp.getProcessor(), 2, 2, 6);
//		ImageStack[] stack = mask1.get_leaf_mask(imp);
//		ImageAccess mask = ImageTools.threshold(new ImageAccess(new ImagePlus("leaf_mask",stack[0])), 170);
		ImageAccess mask = new ImageAccess(IJ.openImage(calibration  +"mask.png"));
//		mask = MorphologicTransform.open(mask, Shape.DISK, 20);
//		mask = MorphologicTransform.complement(mask, 1);
		ridge_im.show();
		mask.show();
		long startTimeTot = System.nanoTime();
		long count = LeafDetector.GradientInit(mask, ridge_im, imp);
		
		
		
		
		long stopTimeTot = System.nanoTime();
		long time = stopTimeTot-startTimeTot;
		IJ.log("Time of execution (ms): " + time/1000000 + " for " + count + " ellipses");
		IJ.log("Per ellipse averaged (us): " + time/1000/count);
	}
}
