package toolbox;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import calibration.PointInter;
import delaunay.Vector2D;
import ij.IJ;
import ij.ImagePlus;

public class EllipseTemplate {
	/*
	 * Radius of big axis x in pixels
	 */
	public double a;
	
	/*
	 * Radius of small axis y in pixels
	 */
	public double b;
	
	/*
	 * Angle of orientation counter-clockwise in radian
	 */
	public double orientation;
	
	/*
	 * Ellipse constructor
	 */
	public EllipseTemplate(double a, double b, double orientation) {
		this.a = a;
		this.b = b;
		this.orientation = orientation;
	}
	
	/*
	 * Return list of pixels on the ellipse longer axis
	 * considering the ellipse centered in {0,0}
	 */
	public static ArrayList<Point2D.Double> get_ridge_pixels(EllipseTemplate ellipse){
		ArrayList<Point2D.Double> list = new ArrayList<Point2D.Double>();
		double tolerance = 0.00001;
		double a = ellipse.a;
		double angle = ellipse.orientation;
		if (Math.abs(angle) <=Math.PI/4) 
			for (double pix=0;pix<a*Math.cos(angle);pix++) {
				Vector2D pixel = new Vector2D(pix, -pix*Math.tan(angle));
				list = ridge_symetry_to_list(list, pixel);
			}
		else if (angle <Math.PI/2-tolerance && angle >Math.PI/4 )
			for (double pix=0;pix<a*Math.sin(angle);pix++) {
				Vector2D pixel = new Vector2D(pix*Math.tan(Math.PI/2-angle), -pix);
				ridge_symetry_to_list(list, pixel);
			}
		else if (angle >-Math.PI/2 && angle <-Math.PI/4 )
			for (double pix=0;pix<a*Math.sin(-angle);pix++) {
				Vector2D pixel = new Vector2D(-pix*Math.tan(Math.PI/2+angle), -pix);
				ridge_symetry_to_list(list, pixel);
			}
		else if (Math.abs(angle) <=Math.PI/2+tolerance && Math.abs(angle) >= Math.PI/2-tolerance) 
			for (double pix=0;pix<a;pix++) {
				Vector2D pixel = new Vector2D(0,pix);
				ridge_symetry_to_list(list, pixel);
			}
		return list;
	}
	/*
	 * Apply symetry transform and save PointInter
	 */
	public static ArrayList<Point2D.Double> ridge_symetry_to_list(ArrayList<Point2D.Double> list, Vector2D pixel){
		list.add(new Point2D.Double(pixel.x, pixel.y));
		pixel = pixel.mult(-1);
		list.add(new Point2D.Double(pixel.x, pixel.y));
		return list;
	}
	
	/*
	 * Return list of PointInter on edge of ellipse centered on {0,0}
	 */
	public static ArrayList<Point2D.Double> get_edge_pixels(EllipseTemplate ellipse) {
		ArrayList<Point2D.Double> edge = new ArrayList<Point2D.Double>();
		double a = ellipse.a;
		double b = ellipse.b;
		double angle = ellipse.orientation;
		double stop = b*Math.sin(Math.atan(b/a));//dx/dy=1
		double start = 0;
		for (double pix=start;pix<stop;pix++) {
			double theta = Math.asin(pix/(b));
			Vector2D pixel = new Vector2D(a * Math.cos(theta), -b * Math.sin(theta));
			edge_symetry_to_list(edge, pixel, angle);
		}
		stop = a*Math.sin(Math.atan(a/b)); //dy/dx=1
		for (double pix=start;pix<stop;pix++) {
			double theta = Math.asin(pix/(a));
			Vector2D pixel = new Vector2D(a * Math.sin(theta), -b * Math.cos(theta));
			edge_symetry_to_list(edge, pixel, angle);
			}
		return edge;
	}
	/*
	 * Return list of Point2D.Double on edge of ellipse centered on {0,0}
	 */
	public static ArrayList<Point2D.Double> get_edge_pixels_bis(EllipseTemplate ellipse) {
		ArrayList<Point2D.Double> edge = new ArrayList<Point2D.Double>();
		double a = ellipse.a;
		double b = ellipse.b;
		double angle = ellipse.orientation;
		double start = 0;
		double e = Math.sqrt(1-(b*b)/(a*a));
		double delta = 2.0;
		for (double theta=start;theta<Math.PI*2;theta+=delta/((a*a)/b*Math.pow(1-e*e*Math.cos(theta)*Math.cos(theta),3.0/2.0))) {
			edge.add(new Point2D.Double(a * Math.cos(theta), -b * Math.sin(theta)));
			IJ.log("Rayon courbure: "+ ((a*a)/b*Math.pow(1-e*e*Math.cos(theta)*Math.cos(theta),3.0/2.0)) + "angle: " + theta*180/Math.PI);
		}
		return edge;
	}
	/*
	 * Apply symetry transform and save PointInter
	 */
	public static ArrayList<Point2D.Double> edge_symetry_to_list(ArrayList<Point2D.Double> list, Vector2D pixel, double angle){
		list.add(new Point2D.Double(rotation(pixel, angle).x, rotation(pixel, angle).y));
		pixel.x *= -1;
		list.add(new Point2D.Double(rotation(pixel, angle).x, rotation(pixel, angle).y));
		pixel.y *= -1;
		list.add(new Point2D.Double(rotation(pixel, angle).x, rotation(pixel, angle).y));
		pixel.x *= -1;
		list.add(new Point2D.Double(rotation(pixel, angle).x, rotation(pixel, angle).y));
return list;
	}
	/*
	 * Apply a rotation of orientation around {0,0} to a pixel
	 */
	public static Vector2D rotation(Vector2D pix, double orientation) {
		double[][] shear1_3 = {{1, -Math.tan(-orientation/2)},{0,1}};
		double[][] shear2 = {{1, 0},{Math.sin(-orientation),1}};
		Matrix m1_3 = new Matrix(shear1_3);
		Matrix m2 = new Matrix(shear2);
		return m1_3.times_Vector2D(m2.times_Vector2D(m1_3.times_Vector2D(pix)));

	}
	
	/*
	 * Plot ellipse centered on {200,200} on imageAccess 
	 */
	public static void draw_pixels(ArrayList<Point2D.Double> pix, ImageAccess im) {
		for (int i=1;i<pix.size();i++) {
			Point2D.Double p_ = pix.get(i-1);
			Point2D.Double p = pix.get(i);
			double val = im.getInterpolatedPixel(p.x+200, p.y+200);
			im.putPixel((int)(p.x+200),(int)(p.y+200), val += 1.0);
			IJ.log("Pixel at loc: " + p.x + " " + p.y);
			double norm = Math.sqrt((p.x-p_.x)*(p.x-p_.x) + (p.y-p_.y)*(p.y-p_.y));
			IJ.log("Norm: "+ norm);
		}
	}
}
