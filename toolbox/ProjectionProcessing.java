package toolbox;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.process.ImageProcessor;

public class ProjectionProcessing {
	
	static int EDGE = 0;
	static int RIDGE = 1;
	
	public ImagePlus[] preprocess(ImagePlus in, int[] xpoints, int[] ypoints, double tolerance) {
		int nx = in.getWidth();
		int ny = in.getHeight();
		ImageProcessor imp  = in.getProcessor();
		
		//get rgv values of important pixels
		int[] leaf_ = new int[3];
		imp.getPixel(xpoints[0],ypoints[0], leaf_);
		int[] vein_ = new int[3];
		imp.getPixel(xpoints[1],ypoints[1],vein_);
		int[] back_ = new int[3];
		imp.getPixel(xpoints[2],ypoints[2],back_);
		
		GenericDialog dlg2 = new GenericDialog("Projection points selected");
		dlg2.addMessage("Leaf color R: " +leaf_[0] + " G: " + leaf_[1] + " B: " + leaf_[0]);
		dlg2.addMessage("Vein color R: " +vein_[0] + " G: " + vein_[1] + " B: " + vein_[0]);
		dlg2.addMessage("Back color R: " +back_[0] + " G: " + back_[1] + " B: " + back_[0]);
		dlg2.showDialog();
		
		Vector3D rgb_leaf = new Vector3D((double)leaf_[0], (double)leaf_[1], (double)leaf_[2]);
		Vector3D rgb_vein = new Vector3D((double)vein_[0], (double)vein_[1], (double)vein_[2]);
		Vector3D rgb_back = new Vector3D((double)back_[0], (double)back_[1], (double)back_[2]);
		
		//OUT IMAGES CREATION
		ImagePlus out[] = new ImagePlus[2];
		out[EDGE] = IJ.createImage("Edge_projection", nx, ny, 1, 32);
		out[RIDGE] = IJ.createImage("Ridge_projection", nx, ny, 1, 32);
		ImageProcessor ip_out[]  = new ImageProcessor[2];
		ip_out[EDGE] = out[EDGE].getProcessor();
		ip_out[RIDGE] = out[RIDGE].getProcessor();
		
		//calcul projection vector
		Vector3D lv = rgb_leaf.diff(rgb_vein);
		Vector3D lb = rgb_back.diff(rgb_leaf);
		Vector3D lv_tol = lv.mult(1.0/tolerance);
		Vector3D lb_tol = lb.mult(1.0/tolerance);
		Vector3D lv__tol = lv.mult(-1.0/tolerance);
		Vector3D lb__tol = lb.mult(-1.0/tolerance);
		
		IJ.log("lb: " + lb.x + " lb-10: " + lb__tol.x + " lb+10: " + lb_tol.x);
		
		for (int i=0; i<nx; i++)
			for(int j = 0; j<ny; j++) {
				int[] pix = new int[3];
				imp.getPixel(i, j, pix);
				Vector3D rgb_pix = new Vector3D(pix[0], pix[1], pix[2]);
				//calcul pixel projection 
				int edge_value = project_pixel(rgb_pix, rgb_leaf.sub(lb_tol), lb.sub(lb__tol));
				int ridge_value = project_pixel(rgb_pix, rgb_leaf.sub(lv_tol), lv.sub(lv__tol));
				ip_out[EDGE].putPixelValue(i, j, edge_value);
				ip_out[RIDGE].putPixelValue(i, j, ridge_value);
			}
		return out;
	}
	
	public int project_pixel(Vector3D p, Vector3D x, Vector3D vector) {
		//project point p on line d with x a point of d and vector the directed vector
		double num = vector.dot(p.sub(x));
		double den = vector.mag();
		int pixel = (int)Math.round(num/den*255);
		
		if (pixel > 255)
			return 255;
		if(pixel < 0)
			return 0;
		return pixel;
	}

//	public double[] vector_calcul(double[] pos1, double[] pos2) {
//		double[] vector = new double [3];
//		vector[0] = (double)(pos2[0]-pos1[0]);
//		vector[1] = (double)(pos2[1]-pos1[1]);
//		vector[2] = (double)(pos2[2]-pos1[2]);
//		return  vector;
//	}
//	
//	public double[] vector_scale(double[] x, double factor) {
//		double[] out = new double [3];
//		for(int i=0; i<3; i++)
//			out[i] = x[i]*factor;
//		return out;
//	}
}









