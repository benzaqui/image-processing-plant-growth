package toolbox;

import java.util.Arrays;
import ij.IJ;
import toolbox.ImageAccess;

public class MorphologicTransform {

	public enum Shape { DISK, SQUARE, CROSS, UNKNOWN };

	private static boolean[][] createStructuringElement(Shape shape, int size) {
		IJ.log("Structuring element " + shape.name() + " (" + size + ")");
		boolean se[][] = new boolean[size][size];
		if (shape == Shape.UNKNOWN) {
			for (int k = 0; k < size; k++)
				se[k][k] = true;
		}
		if (shape == Shape.DISK) {
			int middle = (size-1)/2;
			for (int i=0; i<size;i++) {
				for (int j=0;j<size;j++) {
					if (((i-middle)*(i-middle)+(j-middle)*(j-middle))<=middle*middle) {
						se[i][j]=true;
					}
				}
			}
		}
		if (shape == Shape.SQUARE) {
			for (int i=0; i<size;i++)
				for (int j=0;j<size;j++)
					se[i][j]=true;
		}
		if (shape == Shape.CROSS) {
			int middle = (size-1)/2;
			for (int i=0; i<size;i++)
				for (int j=0;j<size;j++)
					if (i == middle || j == middle)
						se[i][j]=true;
		}
		return se;
	}

	public static ImageAccess dilate(ImageAccess in, Shape shape, int size) {
		boolean se[][] = createStructuringElement(shape, size);
		ImageAccess output = new ImageAccess(in.nx, in.ny);
		for (int x = 0; x < in.nx; x++) {
			for (int y = 0; y < in.ny; y++) {
				double block[][] = in.getNeighborhood(x, y, size, size);
				double max = -Double.MAX_VALUE;
				for (int k = 0; k < size; k++)
					for (int l = 0; l < size; l++) {
						if (se[k][l] == true) {
							if (block[k][l] > max) {
								max = block[k][l];
							}
						}
					}
				output.putPixel(x, y, max);
			}
		}
		return output;
	}

	public static ImageAccess erode(ImageAccess in, Shape shape, int size) {
		boolean se[][] = createStructuringElement(shape, size);
		ImageAccess output = new ImageAccess(in.nx, in.ny);
		for (int x = 0; x < in.nx; x++) {
			for (int y = 0; y < in.ny; y++) {
				double block[][] = in.getNeighborhood(x, y, size, size);
				double min = Double.MAX_VALUE;
				for (int k = 0; k < size; k++)
					for (int l = 0; l < size; l++) {
						if (se[k][l] == true) {
							if (block[k][l] < min) {
								min = block[k][l];
							}
						}
					}
				output.putPixel(x, y, min);
			}
		}
		return output;
	}
	
	public static ImageAccess complement(ImageAccess in, int max_pix) {
		ImageAccess output = new ImageAccess(in.nx, in.ny);
		for (int x = 0; x < in.nx; x++)
			for (int y = 0; y < in.ny; y++)
				output.putPixel(x, y, max_pix-in.getPixel(x, y));
		return output; // TODO change the return image
	}
	public static ImageAccess substract(ImageAccess in1, ImageAccess in2) {
		ImageAccess output = new ImageAccess(in1.nx, in1.ny);
		for (int x = 0; x < in1.nx; x++)
			for (int y = 0; y < in1.ny; y++)
				output.putPixel(x, y, in1.getPixel(x, y)-in2.getPixel(x, y));
		return output; // TODO change the return image
	}

	public static ImageAccess open(ImageAccess in, Shape shape, int size) {
		ImageAccess output = dilate(erode(in, shape,size), shape, size);
		return output; // TODO change the return image
	}

	public static ImageAccess close(ImageAccess in, Shape shape, int size) {
		ImageAccess output = erode(dilate(in, shape,size), shape, size);
		return output; // TODO change the return image
	}

	public static ImageAccess gradient(ImageAccess in, Shape shape, int size) {
		ImageAccess output = substract(dilate(in, shape,size),erode(in, shape,size));
		return output; // TODO change the return image
	}

	public static ImageAccess topHat(ImageAccess in, Shape shape, int size) {
		ImageAccess output = substract(in,open(in, shape,size));
		return output; // TODO change the return image
	}

	public static ImageAccess bottomHat(ImageAccess in, Shape shape, int size) {
		ImageAccess output = substract(close(in, shape,size),in);
		return output; // TODO change the return image
	}
	
//	public static void checkDualityErodeDilate(ImageAccess in, Shape shape, int size) {
//		ImageAccess erosion = complement(erode(in, shape,size),255);
//		ImageAccess dilation = dilate(complement(in,255), shape,size);
//		erosion.show("erosion");
//		dilation.show("dilation");
//	}
//	
//	public static void checkDualityOpenClose(ImageAccess in, Shape shape, int size) {
//		ImageAccess open = open(in, shape,size);
//		ImageAccess close = complement(close(complement(in,255), shape,size),255);
//		open.show("open");
//		close.show("close");
//	}
//	
//	public static ImageAccess median(ImageAccess in) {
//		ImageAccess output = new ImageAccess(in.nx, in.ny);
//		for (int x = 0; x < in.nx; x++) {
//			for (int y = 0; y < in.ny; y++) {
//				double block[][] = in.getNeighborhood(x, y, 3, 3);
//				double array_block[] = new double[9];
//				for (int i = 0; i < 3; i++) {
//					for (int j = 0; j < 3; j++) {
//						array_block[i*3+j] = block[i][j];
//					}
//				}
//			Arrays.sort(array_block);
//			output.putPixel(x, y, array_block[4]);
//			}
//		}
//		return output; // TODO change the return image
//	}
//
//	public static ImageAccess distanceBorderMap(ImageAccess in) {
//		ImageAccess eroded_1 = in;
//		ImageAccess diff = new ImageAccess(in.nx, in.ny);
//		ImageAccess map = new ImageAccess(in.nx, in.ny);
//		int i=1;
//		while (in.getMaximum() !=0) {
//			in = erode(in, Shape.CROSS, 3);
//			diff = substract(eroded_1, in);
//			eroded_1 = in;
//			diff.multiply(i);
//			diff.divide(255);
//			map.add(diff);
//			i++;
//		}
//		return map; // TODO change the return image
//	}
}
