package toolbox;

public class ImageTools {
	public static ImageAccess threshold(ImageAccess imp, int t) {
		ImageAccess out = new ImageAccess(imp.nx, imp.ny);
		for (int i=0; i< imp.nx ; i++)
			for (int j=0; j< imp.ny ; j++) {
				if (imp.getPixel(i, j) > t)
					out.putPixel(i, j, 0);
				else
					out.putPixel(i, j, 255);
			}
		return out;
	}
}
