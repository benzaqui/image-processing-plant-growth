package toolbox;

import Jama.Matrix;
import ij.IJ;

public class PrintTools {
	public void print_2dmatrix(Matrix mat) {
		int column = mat.getColumnDimension();
		int line = mat.getRowDimension();
		double m[][] = mat.getArrayCopy();
		IJ.log("Matrix: ");
		if (column == 9)
			for (int i=0; i<line; i++)
				IJ.log(m[i][0] + " " + m[i][1] + " " + m[i][2] + " " + m[i][3] + " " + m[i][4] + " " + m[i][5] + " " + m[i][6] + " " + m[i][7] + " " + m[i][8]);
		if (column == 3)
			for (int i=0; i<line; i++)
				IJ.log(m[i][0] + " " + m[i][1] + " " + m[i][2]);
		if (column == 1)
			for (int i=0; i<line; i++)
				IJ.log(m[i][0] + " ");
	}
}
