package toolbox;

public class MathTools {
	
	public double norm2(double a, double b) {
		return Math.sqrt(Math.pow(a,2)+Math.pow(b,2));
	}
}
