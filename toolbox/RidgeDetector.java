package toolbox;

public class RidgeDetector {
	public static ImageAccess[] hessian(ImageAccess in, double sigma) {
		ImageAccess hxx = correlate(in, sigma, 2, 0);
		ImageAccess hyy = correlate(in, sigma, 0, 2);
		ImageAccess hxy = correlate(in, sigma, 1, 1); 
		ImageAccess merit = new ImageAccess(in.nx, in.ny); // to store the merit
		ImageAccess dx = new ImageAccess(in.nx, in.ny); // to store the orientation (x)
		ImageAccess dy = new ImageAccess(in.nx, in.ny); // to store the orientation (y)
		/*
		hxx.show("hxx");
		hyy.show("hyy");
		hxy.show("hxy");
		*/
		for (int y = 0; y < in.ny; y++)
		for (int x = 0; x < in.nx; x++) {
			double cxx = hxx.getPixel(x, y);
			double cyy = hyy.getPixel(x, y);
			double cxy = hxy.getPixel(x, y);
			double delta = (cxx - cyy) * (cxx - cyy) + 4.0 * cxy * cxy;
			if (delta >= 0) {
				delta = Math.sqrt(delta);
				double lmin = (cxx + cyy - delta) * 0.5;
				double lmax = (cxx + cyy + delta) * 0.5;
				double m = Math.sqrt(Math.abs(lmin) * Math.abs(lmin - lmax));
				merit.putPixel(x, y, m);
				double vx = hxy.getPixel(x, y);
				double vy = (lmin - hxx.getPixel(x, y));
				double norm = Math.sqrt(vx * vx + vy * vy);
				dx.putPixel(x, y, vx / norm);
				dy.putPixel(x, y, vy / norm);
			}
		}
//		merit.show("merit");
//		dx.show("dx");
//		dy.show("dy");

		return new ImageAccess[] {merit, dx, dy};
	}

	public ImageAccess ridges(ImageAccess in, double sigma, double tL, double tH) {
		ImageAccess[] hessian = hessian(in, sigma);
		ImageAccess nms = applyNonMaximumSuppression(hessian);
//		nms.show("NMS");
		ImageAccess out = HysteresisThreshold.thresholding(nms, tL, tH);
		return out;
	}
	
	private static ImageAccess applyNonMaximumSuppression(ImageAccess in[]) {
		ImageAccess m = in[0];
		ImageAccess ux = in[1];
		ImageAccess uy = in[2];
		int nx = m.getWidth();
		int ny = m.getHeight();
		ImageAccess out = new ImageAccess(nx, ny);
		for (int y = 0; y < ny; y++)
		for (int x = 0; x < nx; x++) {
			double g = m.getPixel(x, y);
			if (g != 0.0) {
				double vx = ux.getPixel(x, y);
				double vy = uy.getPixel(x, y);
				double g1 = m.getInterpolatedPixel(x - vx, y - vy);
				if (g >= g1) {
					double g2 = m.getInterpolatedPixel(x + vx, y + vy);
					if (g >= g2)
						out.putPixel(x, y, g);
				}
			}
		}
		return out;
	}
	private static double[] gaussian(double sigma) {
		int size = (int)(Math.ceil(sigma*3.0)*2.0 + 1.0);
		int hsize = size / 2;
		double kernel[] = new double[size];
		double s2 = sigma*sigma;
		double cst = 1.0 / Math.sqrt(2.0 * Math.PI * s2);
		for(int k=-hsize; k<=hsize; k++) {
			kernel[k+hsize] = cst * Math.exp(-0.5*k*k/s2);
		}
		return kernel;
	}
	
	private static double[] gaussian1(double sigma) {
		double kernel[] = gaussian(sigma);
		int hsize = kernel.length / 2;
		double s2 = sigma*sigma;
		for(int k=0; k<kernel.length; k++) {
			double kc = k - hsize;
			kernel[k] = (-kc/s2) * kernel[k];
		}
		return kernel;
	}
	
	private static double[] gaussian2(double sigma) {
		double kernel[] = gaussian(sigma);
		int hsize = kernel.length / 2;
		double s2 = sigma*sigma;
		for(int k=0; k<kernel.length; k++) {
			double kc = k - hsize;
			kernel[k] = (kc*kc/(s2*s2) - 1.0/s2) * kernel[k];
		}
		return kernel;
	}	

	public static double[][] kernel(double sigmaX, int orderX, double sigmaY, int orderY) {
		double kernelX[];
		switch(orderX) {
			case 1 : kernelX = gaussian1(sigmaX); break;
			case 2 : kernelX = gaussian2(sigmaX); break;
			default : kernelX = gaussian(sigmaX); break;
		}
		double kernelY[];
		switch(orderY) {
			case 1 : kernelY = gaussian1(sigmaX); break;
			case 2 : kernelY = gaussian2(sigmaX); break;
			default : kernelY = gaussian(sigmaY); break;
		}
		
		double kernel[][] = new double[kernelX.length][kernelY.length];
		for(int j=0; j<kernelY.length; j++) 
		for(int i=0; i<kernelX.length; i++)
			kernel[i][j] = kernelX[i] * kernelY[j];
		return kernel;
	}
	public static ImageAccess correlate(ImageAccess in, double sigma, int orderX, int orderY) {
		
		double mask[][] = kernel(sigma, orderX, sigma, orderY);
		int size = mask.length;
		ImageAccess out = new ImageAccess(in.nx, in.ny);
		for (int x=0; x<in.nx; x++)
		for (int y=0; y<in.ny; y++) {
			double block[][] = in.getNeighborhood(x, y, size, size);
			double sum = 0.0;
			for(int j=0; j<size; j++) 
			for(int i=0; i<size; i++) {
				sum += block[i][j] * mask[i][j];
			}
			out.putPixel(x, y, sum);
		}
		return out;
	}
	
	public static ImageAccess get_ridges(ImageAccess in, double sigma) {
		ImageAccess[] hessian = hessian(in, sigma);
		ImageAccess nms = applyNonMaximumSuppression(hessian);
//		nms.show("NMS");
		return nms;
	}
}
