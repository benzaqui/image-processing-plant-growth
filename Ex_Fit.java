import ij.IJ;
import ij.plugin.PlugIn;
import lma.LMA;
import lma.LMAFunction;
import lma.LMAMultiDimFunction;


/** 
 * An example fit which fits a plane to some data points 
 * and prints out the resulting fit parameters.
 */
public class Ex_Fit implements PlugIn {
	/** An example function with a form of y = a0 * x0 + a1 * x1 + a2*/
	public static class MultiDimExampleFunction extends LMAMultiDimFunction {
		@Override
		public double getY(double x[], double[] a) {
			return a[0] * x[0] + a[1] * x[1] + a[2];
		}
		@Override
		public double getPartialDerivate(double x[], double[] a, int parameterIndex) {
			switch (parameterIndex) {
				case 0: return x[0];
				case 1: return x[1];
				case 2: return 1;
			}
			throw new RuntimeException("No such parameter index: " + parameterIndex);
		}
	}
	
	
	public static class CosineFunctionMeanAffine extends LMAFunction
	{
		@Override
	    public double getY(double x, double a[])
	    {
//			IJ.log("x: " + x);
//			IJ.log("a0: " + a[0]);
//			IJ.log("a1: " + a[1]);
//			IJ.log("a2: " + a[2]);
//			IJ.log("a3: " + a[3]);
//			IJ.log("a4: " + a[4]);
	        return a[0] * Math.cos(a[1] *( x - a[2])) + (a[4]*x + a[3]);
	    }
		@Override
	    public double getPartialDerivate(double x, double a[], int parameterIndex)
	    {
	        switch(parameterIndex)
	        {
	        case 0: // '\0'
	            return Math.cos(a[1] *( x - a[2]));

	        case 1: // '\001'
	            return a[0] * (a[2]-x) * Math.sin(a[1] *( x - a[2]));

	        case 2: // '\002'
	            return a[0] * a[1]*Math.sin(a[1] *( x - a[2]));

	        case 3: // '\003'
	            return 1.0D;
	        
	        case 4:
	        	return x;
	        }
	        throw new RuntimeException((new StringBuilder("No such parameter index: ")).append(parameterIndex).toString());
	    }
	}
	/** Does the actual fitting by using the above MultiDimExampleFunction (a plane) */
	@Override
	public void run(String args) {
		LMA lma = new LMA(
			new MultiDimExampleFunction(),
			new double[] {1, 1, 1},
			new double[][] {
				// y x0 x1
				{0, 2, 6},
				{5, 10, 2},
				{7, 20, 4},
				{9, 30, 7},
				{12, 40, 6}
			}
		);
		lma.fit();
		IJ.log("iterations: " + lma.iterationCount);
		IJ.log(
			"chi2: " + lma.chi2 + ",\n" +
			"param0: " + lma.parameters[0] + ",\n" +
			"param1: " + lma.parameters[1] + ",\n" +
			"param2: " + lma.parameters[2]
		);
		LMA lma2 = new LMA(
				new CosineFunctionMeanAffine(),
				new double[] {1, 1, 10, 1.5, 0}, // params (x, y, a, scale, orientation
				new double[][] {
					// y x0 x1
					{1, 1, 1, 1, 1, 1},  // 1,1,1,1,1
					{1, 1, 1, 1, 1, 1}
				}
			);
			lma2.fit();
			IJ.log("iterations: " + lma2.iterationCount);
			IJ.log(
				"chi2: " + lma2.chi2 + ",\n" +
				"param0: " + lma2.parameters[0] + ",\n" +
				"param1: " + lma2.parameters[1] + ",\n" +
				"param2: " + lma2.parameters[2] + ",\n" +
				"param3: " + lma2.parameters[3] + ",\n" +
				"param4: " + lma2.parameters[4]
			);
	}
}

