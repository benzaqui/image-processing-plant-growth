
import java.io.File;
import java.util.ArrayList;

import calibration.PointInter;
import calibration.Constantes.Bool;
import calibration.Constantes.Dir;
import calibration.Constantes.Table;
import calibration.EdgeDetector;
import calibration.HoughTransform;
import calibration.Constantes;

import delaunay.Vector2D;

import ij.IJ;
import ij.ImagePlus;

import ij.plugin.PlugIn;
import ij.process.FloatProcessor;

/*
 * Class to review, need to crate functions
 */
public class Calibration_simple implements PlugIn {
	
	
	@Override
	public void run(String arg0) {
		/*
		 * Define what will be showed to the user, options:
		 * SHOW_ALL_RESULTS -> Show all intermediate images + overlays
		 * SHOW_FINAL_RESULTS -> Show only the final image + overlays
		 * SHOW_NOTHING
		 */
		Constantes.set_bool(Bool.SHOW_ALL_RESULTS);
		
		long startTimeTot = System.nanoTime();
		
		int calib[][] = Constantes.get_table(Table.CALIB);
		int nb_measure = calib.length;
		int dim = calib[0].length;
		
		for (int i=0; i<nb_measure; i++) {
			
			long startTime = System.nanoTime();
			int k = i+1;
			IJ.log("Calib Image: " + k +"/" + nb_measure );
			ImagePlus imp1 = IJ.openImage(Constantes.get_dir(Dir.CALIB) + calib[i][2] +"_cam1.png");
			ImagePlus imp2 = IJ.openImage(Constantes.get_dir(Dir.CALIB) + calib[i][2] +"_cam2.png");
			if (imp1 == null || imp2 == null) {
				IJ.error("Calibration Image " + i + " not found");
				return;
			}
			/*
			 * Process Camera of reference
			 */
			ImagePlus sobel1 = EdgeDetector.sobel_filter(imp1);
			sobel1.setTitle("Edge Detection z=" + calib[i][2] + " camera 1");
			ArrayList<PointInter> inter1 = HoughTransform.hough(sobel1, 20, 360*(int)Constantes.R_FACTOR_HOUGH, 2000, 40, 9, 6, 10);
			
			/*
			 * Process second camera
			 */
			ImagePlus sobel2 = EdgeDetector.sobel_filter(imp2);
			sobel2.setTitle("Edge Detection z=" + calib[i][2] + " camera 2");
			ArrayList<PointInter> inter2 = HoughTransform.hough(sobel2, 20, 360*(int)Constantes.R_FACTOR_HOUGH, 2000, 40, 9, 6, 10);
			
			ArrayList<PointInter> disparity_list = new ArrayList<PointInter>();
			
			float acc[][] = new float[imp1.getWidth()][imp1.getHeight()]; //dimensions of Hough Transform 
			int sum_x = 0;
			int sum_y = 0;
			for(int j=0; j<54; j++) {
				Vector2D pt_1 = new Vector2D(inter1.get(j).x, inter1.get(j).y);
				Vector2D pt_2 = new Vector2D(inter2.get(j).x, inter1.get(j).y);
				Vector2D diff = pt_1.diff(pt_2);
				sum_x += (int)diff.x;
				sum_y += (int)diff.y;
				disparity_list.add(new PointInter((int)diff.x, (int)diff.y));
				acc[(int)diff.x][(int)diff.y] += 1;
			}
			sum_x/=54;
			sum_y/=54;
			calib[i][0] = sum_x;
			calib[i][1] = sum_y;
////			acc[sum_x][sum_y] += 5;
//			IJ.log("disparity X: " + sum_x + " Y: " + sum_y);
//			FloatProcessor dispar = new FloatProcessor(acc);
//			ImagePlus disparity = new ImagePlus("Disparity map z=" + calib[i][2] , dispar);
//			if (Constantes.SHOW_INTER_RESULTS == true)
//				disparity.show();
			long endTime = System.nanoTime();
			long duration = (endTime - startTime);  //divide by 1000000 to get milliseconds.
			IJ.log("Process time (ms): " + duration/1000000);
		}
		
		double mean[] = new double[3];
		for (int j=0; j<3;j++) {
			for (int i=0; i<3; i++)
				mean[j] += calib[i][j];
			mean[j] /= dim;
		}
		double num=0;
		double den=0;
		for (int i=0; i<dim; i++) {
			num += (calib[i][0]-mean[0])*(calib[i][2]-mean[2]);
			den += (calib[i][0]-mean[0])*(calib[i][0]-mean[0]);
		}
		double slope = num/den;
		double ord_origin = mean[2]-slope*mean[0];
		IJ.log("Z="+slope+"X + "+ord_origin);
		
		long endTimeTot = System.nanoTime();
		long duration_tot = (endTimeTot - startTimeTot);  //divide by 1000000 to get milliseconds.
		IJ.log(" ");
		IJ.log("Calibration process time:");
		IJ.log("Total (s): " + duration_tot/1000000000);
		IJ.log("Per pair of images (ms): " + duration_tot/(1000000*nb_measure));
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
//	public Matrix fill_matrix(ArrayList<PointInter> points1, ArrayList<PointInter> points2) {
//		double[][] array = new double[54][9];
//		for(int i=0; i<54; i++) {
//			PointInter pt = points1.get(i);
//			PointInter pt_prim = points2.get(i);
//			array[i][0] = pt.x*pt_prim.x;
//			array[i][1] = pt.y*pt_prim.x;
//			array[i][2] = pt_prim.x;
//			array[i][3] = pt.x*pt_prim.y;
//			array[i][4] = pt.y*pt_prim.y;
//			array[i][5] = pt_prim.y;
//			array[i][6] = pt.x;
//			array[i][7] = pt.y;
//			array[i][8] = 1.0;
//		}
//		return new Matrix(array);
//	}
//	public ArrayList<PointInter> centre_intersections(ArrayList<PointInter> inter, int xsize, int ysize){
//		ArrayList<PointInter> centered = new ArrayList<PointInter>();
//		for (PointInter p : inter)
//			centered.add(new PointInter((int)p.x-xsize/2,(int)p.y-ysize/2));
//		return centered;
//	}
//	public void fmatrix_estimation(ArrayList<PointInter> inter1, ArrayList<PointInter> inter2, ImagePlus imp) {
//		int nx = imp.getWidth(); 
//		int ny = imp.getHeight();
//		PrintTools tool = new PrintTools();
//		if (inter1.size() != 54 && inter2.size() != 54)
//			IJ.log("ERROR: 54 points need to be detected in both images");
//		ArrayList<PointInter> centered1 = centre_intersections(inter1, nx, ny);
//		ArrayList<PointInter> centered2 = centre_intersections(inter2, nx, ny);
//		Matrix A = fill_matrix(centered1, centered2);
//		Matrix V = A.svd().getV();
//		Matrix f = V.getMatrix(0, 8, 8, 8);
//		double [][] f_array = f.getArray();
//		double[][] array = new double[3][3];
//		for(int i=0;i<3;i++)
//			for(int j=0;j<3;j++) {
//				array[i][j] = f_array[i*3+j][0];
//			}
//		Matrix F = new Matrix(array);
//		Matrix D = F.svd().getS(); //F = USV
//		array = D.getArrayCopy();
//		array[2][2] = 0;
//		Matrix D_prim = new Matrix(array);
//		Matrix F_est = F.svd().getU().times(D_prim.times(F.svd().getV().transpose()));
//		tool.print_2dmatrix(A);
//		tool.print_2dmatrix(V);
//		tool.print_2dmatrix(f);
//		tool.print_2dmatrix(F);
//		tool.print_2dmatrix(D);
//		tool.print_2dmatrix(D_prim);
//		tool.print_2dmatrix(F_est);
//	}
	
}