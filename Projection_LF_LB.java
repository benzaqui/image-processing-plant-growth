

import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.gui.PointRoi;
import ij.gui.Roi;
import ij.plugin.PlugIn;

import toolbox.ProjectionProcessing;
/*
 * TODO: Add error function if the user click too many times or if intensities are not right for projection
 */
public class Projection_LF_LB implements PlugIn{

	@Override
	public void run(String arg0) {
		
		ImagePlus imp = IJ.getImage();
		if (imp == null) {
			IJ.error("Open an image");
			return;
		}
		GenericDialog dlg = new GenericDialog("Projection selector");
		dlg.addNumericField("tolerance (%)", 10, 0);
		dlg.addMessage("Be sure you clicked on plant parts in following order");
		dlg.addMessage("Darker part of Leaf (1)");
		dlg.addMessage("Brighter part of Leaf (Vein) (2)");
		dlg.addMessage("Darker part of Background (3)");
		dlg.showDialog();
		if (dlg.wasCanceled())
			return;
		double tolerance = dlg.getNextNumber();
		IJ.log("tolerance" + tolerance);
		
		//new WaitForUserDialog("User information", "Click on leaf.").show();
		
		int xpoints[] = null;
		int ypoints[] = null;
		Roi rois = imp.getRoi();
		//Define manually point of interest
		if (rois instanceof PointRoi) {
			xpoints = rois.getPolygon().xpoints;
			ypoints = rois.getPolygon().ypoints;
		}else {
			IJ.log("Use multi-point selector tool to click on part of image and start the plugin again:");
			return;
		}
		for (int i=0; i<xpoints.length; i++)
			IJ.log("pts" + xpoints[i] + " " + ypoints[i]);
		ProjectionProcessing preprocessing = new ProjectionProcessing();
		ImagePlus projection[] = preprocessing.preprocess(imp, xpoints, ypoints, tolerance);
		projection[0].getProcessor().resetMinAndMax();
		projection[0].show();
		projection[1].getProcessor().resetMinAndMax();
		projection[1].show();
	}

}
