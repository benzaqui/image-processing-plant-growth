import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import calibration.PointInter;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import ij.process.FloatProcessor;
import toolbox.EllipseTemplate;
import toolbox.ImageAccess;
import toolbox.ImageAccess.Display;
import toolbox.MorphologicTransform;
import toolbox.MorphologicTransform.Shape;
import toolbox.differential;
import mpicbg.ij.*;
import mpicbg.imagefeatures.FloatArray2DSIFT.Param;
import sift.SIFT_ExtractPointRoi;

public class Test_Scripts implements PlugIn{

	@Override
	public void run(String arg0) {
//		new SIFT_ExtractPointRoi().run("");
//		new differential().run("");
		
//		ImageAccess im = new ImageAccess(400,400);
//		EllipseTemplate el = new EllipseTemplate(150,100, 0);
////		Ellipse2D.Double ellipse = new Ellipse2D.Double(150,100, 0, 0);
//		ArrayList<Point2D.Double> edge = EllipseTemplate.get_edge_pixels_bis(el);
//		EllipseTemplate.draw_pixels(edge, im);
//		im.show();
		
		ImageAccess in = new ImageAccess(IJ.getImage());
		FloatProcessor fp = in.createFloatProcessor();
//		stack.addSlice("", fp);
//		display(title, stack);
		for (int i=2; i<4; i+=2) {
			correlate(in, (double)i, 2, 0).show("hxx"+i);
			correlate(in, (double)i, 0, 2).show("hyy"+i);
			correlate(in, (double)i, 1, 0).show("hx"+i);
			correlate(in, (double)i, 0, 1).show("hy"+i);
		}
		
		
		
		//InteractiveMapping a = new InteractiveMapping();
		//SIFT.addFields(gd, p);
//		ImagePlus image = IJ.getImage();
//		IJ.run(image, "8-bit", "");
//		ImageAccess imp = new ImageAccess(image);
//		MorphologicTransform morphtrans  = new MorphologicTransform();
//		imp = Depth_Map.threshold(imp, 170);
//		imp.show("thresholded");
//		MorphologicTransform morph = new MorphologicTransform();
//		imp = morph.topHat(imp, Shape.CROSS, 10);
//		imp.show("tophat");
//		imp = morph.bottomHat(imp, Shape.CROSS, 10);
//		imp.show("Bottomhat");
	}
	private double[] gaussian(double sigma) {
		int size = (int)(Math.ceil(sigma*3.0)*2.0 + 1.0);
		int hsize = size / 2;
		double kernel[] = new double[size];
		double s2 = sigma*sigma;
		double cst = 1.0 / Math.sqrt(2.0 * Math.PI * s2);
		for(int k=-hsize; k<=hsize; k++) {
			kernel[k+hsize] = cst * Math.exp(-0.5*k*k/s2);
		}
		return kernel;
	}
	
	private double[] gaussian1(double sigma) {
		double kernel[] = gaussian(sigma);
		int hsize = kernel.length / 2;
		double s2 = sigma*sigma;
		for(int k=0; k<kernel.length; k++) {
			double kc = k - hsize;
			kernel[k] = (-kc/s2) * kernel[k];
		}
		return kernel;
	}
	
	private double[] gaussian2(double sigma) {
		double kernel[] = gaussian(sigma);
		int hsize = kernel.length / 2;
		double s2 = sigma*sigma;
		for(int k=0; k<kernel.length; k++) {
			double kc = k - hsize;
			kernel[k] = (kc*kc/(s2*s2) - 1.0/s2) * kernel[k];
		}
		return kernel;
	}	

	public double[][] kernel(double sigmaX, int orderX, double sigmaY, int orderY) {
		double kernelX[];
		switch(orderX) {
			case 1 : kernelX = gaussian1(sigmaX); break;
			case 2 : kernelX = gaussian2(sigmaX); break;
			default : kernelX = gaussian(sigmaX); break;
		}
		double kernelY[];
		switch(orderY) {
			case 1 : kernelY = gaussian1(sigmaX); break;
			case 2 : kernelY = gaussian2(sigmaX); break;
			default : kernelY = gaussian(sigmaY); break;
		}
		
		double kernel[][] = new double[kernelX.length][kernelY.length];
		for(int j=0; j<kernelY.length; j++) 
		for(int i=0; i<kernelX.length; i++)
			kernel[i][j] = kernelX[i] * kernelY[j];
		return kernel;
	}
	public ImageAccess correlate(ImageAccess in, double sigma, int orderX, int orderY) {
		
		double mask[][] = kernel(sigma, orderX, sigma, orderY);
		int size = mask.length;
		ImageAccess out = new ImageAccess(in.nx, in.ny);
		for (int x=0; x<in.nx; x++)
		for (int y=0; y<in.ny; y++) {
			double block[][] = in.getNeighborhood(x, y, size, size);
			double sum = 0.0;
			for(int j=0; j<size; j++) 
			for(int i=0; i<size; i++) {
				sum += block[i][j] * mask[i][j];
			}
			out.putPixel(x, y, sum);
		}
		return out;
	}
}
